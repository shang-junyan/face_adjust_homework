# face_adjust_homework

### 项目描述
项目通过加载预训练的StyleGAN模型和相应的调整方向向量，用户可以在界面上通过滑块调节人脸图像的年龄和微笑程度。用户可以上传自己的图片作为输入，或者使用预先提供的示例图片。然后，系统将根据用户的输入调整潜在向量，通过生成器网络生成新的人脸图像，并在界面上显示输出结果。

### 软件架构
这是一个Gradio项目，通过调节滑动条，可以调节人脸的年龄和微笑的程度

### 项目运行效果截图
![输入图片说明](%E8%BF%90%E8%A1%8C%E6%88%AA%E5%9B%BE.png)

### 依赖
- Python 3
- Tensorflow
- Gradio
- Numpy
- OpenCV
- Pillow
- Math
- Pytorch

### 使用
1.克隆项目到本地
2.创建conda虚拟环境
3.安装依赖、配置环境
4.下载"stylegan-1024px-new.model"模型并创建model文件夹，将模型放入models文件夹
5.运行项目 app.py
6.在浏览器中打开 http://localhost:7860/

### 注意
要自行下载 stylegan-1024px-new.model 模型并放入models文件夹涉及到数据的输入输出或者模型训练，确保数据路径正确配置
确保数据文件格式正确且路径合法

### 个人信息

- 学号: 202152320125
- 年级: 2021
- 专业: 智能科学与技术
- 班级: 一班
